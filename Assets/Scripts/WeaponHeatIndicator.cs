﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Slider))]
public class WeaponHeatIndicator : MonoBehaviour {


	public void SetIndicator(float value)
    {
        GetComponent<Slider>().value = value;
    }

}
