﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum WeaponType { PARTICLE, PROJECTILE, SPECIAL}
[RequireComponent(typeof(AudioSource))]
public class Weapon : MonoBehaviour {

    [SerializeField] protected int damageMin=1;
    [SerializeField] protected int damageMax = 5;
    [SerializeField] protected AudioClip fireSound;
    [SerializeField] protected AudioClip cantFireSound;
    [SerializeField] protected WeaponType weaponType=WeaponType.PARTICLE;

    protected AudioSource audioSource;
    protected bool overheatIssued = false;
	// Use this for initialization
	void Start ()
    {
        Init();
    }

    virtual protected void Init()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public virtual int DamageAmount()
    {
        return Random.Range(damageMin, damageMax + 1);
    }

    protected virtual void StartFireSound()
    {
        if (fireSound!=null)
        {
            audioSource.PlayOneShot(fireSound);
        }
    }

    protected virtual void StopFireSound()
    {
        audioSource.Stop();
    }
	
    protected virtual void StartCantFireSound()
    {
        if (cantFireSound)
        {
            audioSource.PlayOneShot(cantFireSound);
        }
    }

     public void OnFireWeapon(WeaponType weapon)
    {
        if(weaponType==weapon)
            if(!overheatIssued)
        {
            FireWeapon();
        }
    }
    
    public void OnStopFireWeapon(WeaponType weapon)
    {
        if (weaponType == weapon)
            overheatIssued = false;
        {
            StopFireWeapon();
            StopFireSound();
        }
    }

    public void OnStopFiringAllWeapons()
    {
        overheatIssued = false;
        StopFireWeapon();
        StopFireSound();
    }

    virtual protected void FireWeapon()
    {

    }

    virtual protected void StopFireWeapon()
    {

    }

	// Update is called once per frame
	void Update () {
		
	}
}
