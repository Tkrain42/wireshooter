﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CollisionHandler : MonoBehaviour {

    [Range(1, 10)]
    [SerializeField]
    float levelLoadDelay = 2.0f;


    private void OnTriggerEnter(Collider other)
    {
        Enemy enemy = other.GetComponent<Enemy>();
        if (enemy)
        {
            if (!enemy.isAlive)
                return;
        }
        BeginKillPlayerSequence();
    }

    private void BeginKillPlayerSequence()
    {
        SendMessageUpwards("onPlayerIsDead");
        TagBasedParticleActivator.BroadcastTagBasedTrigger("playerdeath");
        Invoke("Die", levelLoadDelay);
 
        
    }
    // Update is called once per frame
    private void Die()
    {
        SceneManager.LoadScene(0);
    }
}
