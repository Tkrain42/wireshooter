﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Splash: MonoBehaviour {
    [SerializeField] string nextLevel = "Level1";
    [SerializeField] float delay = 3.0f;
    float elapsed = 0.0f;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        elapsed += Time.deltaTime;
        if (elapsed > delay)
        {
            SceneManager.LoadScene(nextLevel);
        }
	}
}
