﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class TagBasedParticleActivator : MonoBehaviour {
    [SerializeField]
    string triggerTag = "any";
    [Tooltip("By default, this class plays a particle system.  It can, however, be set to stop it by checking this box.")]
    [SerializeField]
    bool stop = false;

    public void onTagBasedTrigger(string tag)
    {
        if (triggerTag == tag)
            if (stop)
            {
                StopParticleSystem();
            }
            else
            {
                StartParticleSystem();
            }
    }

    private void StartParticleSystem()
    {
        GetComponent<ParticleSystem>().Play();
        AudioSource audiosource = GetComponent<AudioSource>();
        if (audiosource)
        {
            audiosource.Play();
        }
    }

    private void StopParticleSystem()
    {
        GetComponent<ParticleSystem>().Stop();
        AudioSource audiosource = GetComponent<AudioSource>();
        if (audiosource)
        {
            audiosource.Stop();
        }
    }

    public static void BroadcastTagBasedTrigger(string tag)
    {
        var triggers = FindObjectsOfType<TagBasedParticleActivator>();
        foreach (TagBasedParticleActivator trigger in triggers)
        {
            trigger.SendMessage("onTagBasedTrigger", tag);
        }
    }

}
