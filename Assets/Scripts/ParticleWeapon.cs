﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class ParticleWeapon : Weapon {

    [SerializeField] float overHeatingTemperature = 20f;
    [SerializeField] float soundCycle = 1f;
    [SerializeField] GameObject hitExplosion;
    protected ParticleSystem particleSystem;
    protected WeaponHeatIndicator weaponHeatIndicator;
    protected bool isFiring = false;
    float cycleTimer = 0;
    float currentTemperature;

	// Use this for initialization
	void Start () {
        Init();
	}

    protected override void Init()
    {
        base.Init();
        weaponHeatIndicator = FindObjectOfType<WeaponHeatIndicator>();
        particleSystem = GetComponent<ParticleSystem>();
        particleSystem.Stop();
    }

    protected override void StartFireSound()
    {
        base.StartFireSound();
        audioSource.loop = true;
    }

    protected override void FireWeapon()
    {
        if(currentTemperature>overHeatingTemperature*.75f)
        {
            WarnOverheated();
        }
        else
        {
            particleSystem.Play();
            isFiring = true;
            StartFireSound();
            cycleTimer = 0;
            base.FireWeapon(); 
        }
    }

    private void WarnOverheated()
    {
        if(!overheatIssued)
        if (cantFireSound)
        {
            transform.parent.BroadcastMessage("AudioToastPlayOneShot", cantFireSound);
        }
        overheatIssued = true;
    }

    protected override void StopFireWeapon()
    {
        particleSystem.Stop();
        isFiring = false;
        //base.StopFireWeapon();
    }

    private void Update()
    {
        if (isFiring)
        {
            currentTemperature += Time.deltaTime;
            if (currentTemperature > overHeatingTemperature)
            {
                StopFireWeapon();
                WarnOverheated();
            }
            else
            {
                cycleTimer += Time.deltaTime;
                if (cycleTimer > soundCycle)
                {
                    StartFireSound();
                    cycleTimer = 0;
                } 
            }
        }
        else
        {
            currentTemperature -= Time.deltaTime;
            if (currentTemperature <= float.Epsilon)
                currentTemperature = 0;
        }
        weaponHeatIndicator.SetIndicator(currentTemperature / overHeatingTemperature);
    }

    private void OnParticleCollision(GameObject other)
    {
        //print("OnParticleCollision " + other.name);
        List<ParticleCollisionEvent> events = new List<ParticleCollisionEvent>();
        ParticlePhysicsExtensions.GetCollisionEvents(particleSystem, other, events);
        Vector3 lastBlastPoint = transform.position;
        Enemy enemy = other.GetComponent<Enemy>();
        foreach (ParticleCollisionEvent collisionevent in events)
        {
            if (hitExplosion)
            {
                lastBlastPoint = collisionevent.intersection;
                Instantiate(hitExplosion, lastBlastPoint, Quaternion.identity);
                if (enemy)
                {
                    enemy.TakeDamage(DamageAmount(), lastBlastPoint);
                }

            }
        }
    }

}
