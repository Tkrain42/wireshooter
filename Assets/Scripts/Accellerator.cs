﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Utility;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.Timeline;
using UnityEngine.Playables;

[RequireComponent(typeof(PlayableDirector))]
public class Accellerator : MonoBehaviour {

    float initialSpeed;
    PlayableDirector progressTracker;
	// Use this for initialization
	void Start () {
        progressTracker = GetComponent<PlayableDirector>();
        initialSpeed = 1.0f;
	}
	
	// Update is called once per frame
	void Update () {
        float thrust = CrossPlatformInputManager.GetAxis("Thrust");
        
        progressTracker.playableGraph.GetRootPlayable(0).SetSpeed(thrust);

    }
}
