﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Use this to disable all colliders on this object.
/// </summary>
public class ColliderDisabler : MonoBehaviour {

	/// <summary>
    /// Disables all colliders that are triggers
    /// </summary>
    public void DisableTriggers()
    {
        var colliders = GetComponents<Collider>();
        foreach (Collider collider in colliders)
        {
            if (collider.isTrigger)
            {
                collider.enabled = false;
            }
        }
        colliders = GetComponentsInChildren<Collider>();
        foreach (Collider collider in colliders)
        {
            if (collider.isTrigger)
            {
                collider.enabled = false;
            }
        }
    }

    /// <summary>
    /// Disables all colliders that are NOT triggers
    /// </summary>
    public void DisableColliders()
    {
        var colliders = GetComponents<Collider>();
        foreach (Collider collider in colliders)
        {
            if (!collider.isTrigger)
            {
                collider.enabled = false;
            }
        }
        colliders = GetComponentsInChildren<Collider>();
        foreach (Collider collider in colliders)
        {
            if (!collider.isTrigger)
            {
                collider.enabled = false;
            }
        }
    }

    public void EnableAll()
    {
        var colliders = GetComponents<Collider>();
        foreach (Collider collider in colliders)
        {
            collider.enabled = true;
        }
    }

    /// <summary>
    /// Disables all colliders period, end of sentence.
    /// </summary>
    public void DisableAll()
    {
        var colliders = GetComponents<Collider>();
        foreach (Collider collider in colliders)
        {
            collider.enabled = false;
        }
        colliders = GetComponentsInChildren<Collider>();
        foreach (Collider collider in colliders)
        {
            collider.enabled = false;
        }
    }
}
