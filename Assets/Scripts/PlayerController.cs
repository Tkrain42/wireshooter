﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerController : MonoBehaviour {

    //Todo: Extend these into properties, so that powerups can make the ship more maneuverable.
    [Header("Movement Speed")]
    [Tooltip("in Meters/Second")]
    [Range(2,8)]
    [SerializeField]
    float xSpeed = 4f;
    [Tooltip("in Meters/Second")]
    [Range(2, 8)]
    [SerializeField]
    float ySpeed = 4f;

    [Header("Movement Constraints")]
    [Tooltip("Set to constrain ship to the screen.")]
    [SerializeField]
    [Range(2,8)]    
    float xConstraint = 5f;
    [Tooltip("Set to constrain ship to the screen.")]
    [Range(2, 8)]
    [SerializeField]
    float yConstraint = 4f;

    [Header("Rotation Settings")]
    [Range(0,30)]
    [SerializeField] float pitchFactor = 5.0f;
    [Range(0, 30)]
    [SerializeField] float yawFactor = 5.0f;
    [Range(0, 30)]
    [SerializeField] float rollThrowFactor = 20f;
    [Range(0, 30)]
    [SerializeField] float pitchThrowFactor = 10f;
    [Range(0, 30)]
    [SerializeField] float yawThrowFactor = 10f;

    bool isAlive = true;

    Rigidbody GetRigidbody
    {
        get
        {
            return (GetComponent<Rigidbody>());
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (isAlive)
        {
            float xThrow = CrossPlatformInputManager.GetAxis("Horizontal");
            float yThrow = CrossPlatformInputManager.GetAxis("Vertical");
            ProcessTranslation(xThrow, yThrow);
            ProcessRotation(xThrow, yThrow);
            ProcessFiring();
        }
    }

    /// <summary>
    /// Move player's X,Y position within the window based on the throw.  XSpeed and YSpeed determine how fast the ship can maneuver.
    /// </summary>
    /// <param name="xThrow"></param>
    /// <param name="yThrow"></param>
    private void ProcessTranslation(float xThrow, float yThrow)
    {

        Vector3 pos = transform.localPosition;
        float xOffset = xThrow * xSpeed * Time.deltaTime;
        float yOffset = yThrow * ySpeed * Time.deltaTime;
        pos.x = Mathf.Clamp(pos.x + xOffset, -xConstraint, xConstraint);
        pos.y = Mathf.Clamp(pos.y + yOffset, -yConstraint, yConstraint);
        transform.localPosition = pos;
    }

    private void ProcessRotation(float xThrow, float yThrow)
    {

        Vector3 pos = transform.localPosition;
        float pitch = -pos.y * pitchFactor - yThrow * pitchThrowFactor;
        float yaw = pos.x * yawFactor + xThrow * yawThrowFactor;
        float roll = -xThrow * rollThrowFactor;//+GetComponentInParent<TrackDelta>().Delta*rollThrowFactor;

        transform.localRotation = Quaternion.Euler(pitch, yaw, roll);
    }

    private void ProcessFiring()
    {
        var weapons = GetComponentsInChildren<Weapon>();
        if (CrossPlatformInputManager.GetButtonDown("Fire1"))
        {
            foreach(Weapon weapon in weapons)
            {
                weapon.OnFireWeapon(WeaponType.PARTICLE);
            }
        }
        if (CrossPlatformInputManager.GetButtonUp("Fire1"))
        {
            foreach(Weapon weapon in weapons)
            {
                weapon.OnStopFireWeapon(WeaponType.PARTICLE);
            }
        }
    }

    public void onPlayerIsDead()
    {
        isAlive =false;
        GetRigidbody.isKinematic = false;
        GetRigidbody.useGravity = true;
        
    }

}
