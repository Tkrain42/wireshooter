﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Simple class to drop onto any object you want to survive level loads.

public class StayAlive : MonoBehaviour {

	// Use this for initialization
	void Start () {
        DontDestroyOnLoad(gameObject);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
