﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackDelta : MonoBehaviour {

    Vector3 lastRotation;
    float delta;


    public float Delta { get { return delta; } }

	// Use this for initialization
	void Start () {

	}
	


	// Update is called once per frame
	void Update () {
        Vector3 newRotation = transform.rotation.eulerAngles;
        delta = newRotation.y - lastRotation.y;

        lastRotation = newRotation;
        newRotation.z = 0;
        transform.rotation = Quaternion.Euler(newRotation);
	}
}
