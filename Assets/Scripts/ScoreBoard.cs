﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class ScoreBoard : MonoBehaviour {

    int score = 0;
    Text scoreText;
    float runningScore;

    public int Score {
        get
        {
            return score;
        }
        set
        {
            score = Mathf.Clamp(value, 0, int.MaxValue);
        }
    }
	// Use this for initialization
	void Start () {
        scoreText = GetComponent<Text>();
        scoreText.text = "0";
        
	}
	

	// Update is called once per frame
	void Update () {
        if ((int)runningScore != score)
        {
            runningScore += 1+(score - runningScore)*Time.deltaTime;
            int displayScore=(int)runningScore;
            scoreText.text = displayScore.ToString();
        }
	}
}
