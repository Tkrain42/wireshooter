﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayParticle : MonoBehaviour {

    ParticleSystem particleSystem;
    AudioSource audioSource;

	// Use this for initialization
	void Start () {
        particleSystem = GetComponent<ParticleSystem>();
        audioSource = GetComponent<AudioSource>();
	}
	
    public void OnPlayParticle()
    {
        if (particleSystem)
        {
            particleSystem.Play();
        }
        if (audioSource)
        {
            audioSource.Play();
        }
    }
	// Update is called once per frame

}
