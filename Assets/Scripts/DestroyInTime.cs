﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyInTime : MonoBehaviour {

    [SerializeField] float lifespan = 6.0f;

	// Use this for initialization
	void Start () {
        //Invoke("Die", lifespan);
	}

    private void OnEnable()
    {
        Invoke("Die", lifespan);
    }
    //Repurposed, we completely need to redo this particular code as a new renamed object
    //TODO: Fix this 
    void Die()
    {
        var enemies = GetComponentsInChildren<Enemy>();
        foreach(Enemy enemy in enemies)
        {
            enemy.ResetEnemy();
        }
    }

}
