﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

[RequireComponent(typeof(Rigidbody))]
public class Enemy : MonoBehaviour {

    [SerializeField] int hitPoints = 5;
    [SerializeField] int scoreHitValue = 5;
    [SerializeField] int scoreDestroyValue = 100;
    [SerializeField] GameObject hitExplosion;
    [SerializeField] GameObject deathExplosion;
    int currentHitPoints;
    public bool isAlive
    {
        get
        {
            return currentHitPoints > 0;
        }
    }

    Rigidbody rigidbody;
    // Use this for initialization
    void Start () {
        rigidbody = GetComponent<Rigidbody>();
        currentHitPoints = hitPoints;

	}


   
    public void TakeDamage(int damageAmount, Vector3 hitLocation)
    {

        if (isAlive)
        {
            FindObjectOfType<ScoreBoard>().Score += scoreHitValue;
            currentHitPoints -= damageAmount;
            if (currentHitPoints < 1)
            {
                StartDeathSequence(hitLocation);
            } 
        }
    }


    void StartDeathSequence(Vector3 blastSite)
    {
        var timeline = GetComponent<PlayableDirector>();

        if (timeline)
        {
            timeline.enabled=false;
        }
        FindObjectOfType<ScoreBoard>().Score += scoreDestroyValue;
        GetComponent<ColliderDisabler>().DisableTriggers();
        Animator animator = GetComponent<Animator>();
        if(animator)
        {
            animator.enabled = false;
            
        }
        var particles = GetComponentsInChildren<PlayParticle>();
        Instantiate(deathExplosion, transform.position, Quaternion.identity);
        rigidbody.isKinematic = false;
        rigidbody.useGravity = true;
        rigidbody.AddExplosionForce(100000f, blastSite, 100f);
        Invoke("ResetEnemy", 45);
    }

    void AltDeathSequence()
    {
        Instantiate(deathExplosion, transform.position, Quaternion.identity);
        FindObjectOfType<ScoreBoard>().Score += scoreDestroyValue;
        Destroy(gameObject);
        
    }

    public void ResetEnemy()
    {
        GetComponent<ColliderDisabler>().EnableAll();
        EnableRenderers();
        rigidbody.isKinematic = true;
        rigidbody.useGravity = false;
        Animator animator = GetComponent<Animator>();
        if (animator)
        {
            animator.enabled = true;
        }
        var timeline = GetComponent<PlayableDirector>();
        if (timeline)
        {
            timeline.enabled = true;
        }
        hitPoints += Random.Range(1, 3);
        currentHitPoints = hitPoints;
    }

    void Die()
    {
        //HideRenderers();
    }

    private void HideRenderers()
    {
        var renderers = GetComponents<MeshRenderer>();
        foreach (MeshRenderer renderer in renderers)
        {
            renderer.enabled = false;
        }
    }

    private void EnableRenderers()
    {
        var renderers = GetComponents<MeshRenderer>();
        foreach (MeshRenderer renderer in renderers)
        {
            renderer.enabled = true;
        }
    }
}
