﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicPlayer : MonoBehaviour {

    // Use this for initialization
    private void Awake()
    {
        if (FindObjectsOfType<MusicPlayer>().Length > 1)
        {
            Die();
            return;
        } else
        DontDestroyOnLoad(gameObject);

    }
    private void Start()
    {
        GetComponent<AudioSource>().Play();
    }
    void Die()
    {
        Destroy(gameObject);
    }
}
